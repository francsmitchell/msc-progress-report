\documentclass[12pt,notitlepage,a4]{report}
\usepackage[utf8]{inputenc}
\usepackage{siunitx}
\usepackage{braket}
\usepackage{tikz-feynman}
\tikzfeynmanset{compat=1.0.0}
\usepackage[a4paper, total={7.5in, 10in}]{geometry}
\usepackage{hyperref,graphicx,titlesec,amssymb}
\usepackage{setspace}
\usepackage[width=0.9\textwidth]{caption}
\usepackage{subcaption}
\usepackage[bibstyle=ieee,citestyle=numeric-comp,sorting=none,backend=biber,maxnames=2,minnames=1,maxbibnames=2]{biblatex}
\addbibresource{progress_report.bib}
\graphicspath{ {images/} }
\renewcommand{\bibname}{References}
\onehalfspacing

\DeclareSourcemap{
 \maps[datatype=bibtex,overwrite=true]{
  \map{
    \step[fieldsource=Collaboration, final=true]
    \step[fieldset=usera, origfieldval, final=true]
  }
 }
}

\renewbibmacro*{author}{%
  \iffieldundef{usera}{
    \printnames{author}
  }{
    \printfield{usera} collaboration
  }
}

\titleformat{\chapter}[block]
  {\normalfont\Large\bfseries}{\thechapter.}{1em}{\Large}
\titlespacing*{\chapter}{0pt}{0pt}{10pt}

\titleformat{\section}[block]
 {\normalfont\large\bfseries}{\thesection.}{1em}{\large}
\titlespacing*{\section}{0pt}{4pt}{6pt}

\titleformat{\subsection}[block]
 {\normalfont\normalsize\bfseries}{\thesection.}{1em}{\normalsize}
\titlespacing*{\subsection}{0pt}{0pt}{0pt}

\usepackage{etoolbox}
\makeatletter
\patchcmd{\chapter}{\if@openright\cleardoublepage\else\clearpage\fi}{}{}{}
\makeatother


\title{
{Probing new physics with LHC measurements}\\
{\large Francis Mitchell}\\
{\large 22202940}\\
{\large University College London}\\
{\large MSc project progress report}\\
}
\date{February 2023}

%%% BEGIN DOCUMENT
\begin{document}

\maketitle

\chapter{Introduction}

The observation of the Higgs boson in 2012 \cite{CMS:2012qbp,ATLAS:2012yve} was a monumental step towards completing the standard model of particle physics, yet there are still many open questions in physics. A stark problem lies in that the amount of CP violation observed in processes does not satisfy the Sakharov conditions for the matter-antimatter asymmetry observed in nature \cite{Sakharov:1967dj}. There is also plenty of compelling evidence for the existence of dark matter, yet none has ever been directly observed. Neutrinos are known not to be massless, yet through what mechanism do they acquire this mass?

Theoretical frameworks, such as supersymmetry and effective field theories, have been proposed to answer some of these mysteries. When testing a new theory, it is important to check that its predictions agree with experimental observations. In my research essay, I will outline Contur (\textit{Constraints On New Theories Using Rivet}), a tool, as its name suggests, for setting constraints on new theories \cite{Butterworth:2016sqg}. I will then briefly outline three models that could work with Contur, one of which I will study in depth and investigate using Contur during the summer period of my project.


\chapter{Contur}

As outlined in the introduction, there are many reasons to believe that Beyond Standard Model (BSM) physics exists. Regardless of the existence of BSM physics, it is necessary to extract clear and generic information about physics from the present and future energy regimes achieved at the LHC.

Contur is a tool to set constraints on where new physics can lie. Contur exploits a number of properties to survey existing measurements to set constraints on new physics. The first is that Monte Carlo generators allow for the rapid prediction of model impact on a variety of final states. The second is that the lack of success of popular BSM models, such as supersymmetry, has prompted the move to simplified, more general models that are still able to incorporate new particles. The third property is that LHC precision measurements minimise model dependence and can be analysed and compared using Rivet, a Monte Carlo event generator validation system \cite{Buckley:2010ar}. 

When introducing a new model to explain one particular signature, the model might have unexpected consequences in an already precisely measured final state. By assuming that the uncertainty on these precision measurements is the range within which BSM signatures must lie, Contur can test new theories against these measurements. Contur uses a library of precision measurements provided by Rivet to set constraints.

To avoid extrapolation into unmeasured reasons, where theoretical assumptions are made, Contur requires that cross sections be measured in kinematic regions closely matching the detector acceptance.

In preparation for the summer portion of the project, I have been familiarising myself with using Contur. Following the tutorial, I generated 200 events using Herwig, a Monte Carlo event generator, for a simplified dark matter model (s-channel, spin 1). I analysed the results using Contur and generated exclusion plots, with an illustrative example shown in figure \ref{contur_fig}.

\begin{figure}
	\centering
	\includegraphics[width=70mm]{contur_plot}
	\centering
	\caption{Plot generated using Contur, "Exclusion with data as background from this pool: 76.88\%". My next task is to be able to better interpret plots like these.}
	\label{contur_fig}
\end{figure}

\chapter{New physics models}

In preparation for the June start of the project, I have been reading into three BSM models: dark matter production with a t-channel mediator, W boson mass correction, and Drell-Yan production in third-generation gauge vector leptoquark models at NLO+PS in QCD.

\section{Dark matter production with a t-channel mediator} 

The DMSimpt model \cite{Arina:2020udz} is a general framework for simplified t-channel dark matter models. The leading order Feynman diagram for the production of a pair of dark matter particles is shown in figure \ref{dm_feynman}.

\begin{figure}
	\centering
         \feynmandiagram [vertical=a to b] {
		i1 [particle=\(\bar{q}\)] -- [anti fermion] a -- [ghost] i2 [particle=\(X\)],
		a -- [anti charged scalar, edge label=\(Y\)] b,
		f1 [particle=\(X\)] -- [ghost] b -- [anti fermion] f2 [particle=\(q\)],
		i1 -- [opacity=0] -- f2,
		i2 -- [opacity=0] -- f1
		};
	\caption{Production of a pair of dark matter particles via a dark matter mediator.}
	\label{dm_feynman}
\end{figure}

The model I have been looking at is highly configurable, allowing for different spins of the dark matter particles (0, $\frac{1}{2}$, or 1), self- or non self-conjugate dark matter particles, and assumptions about which SM quarks the dark matter particles can couple to. Parameters, such as the dark matter particle mass, mediator mass, and coupling strength are also not constrained.

I plan to study the "S3D\_uR" configuration of this model in Contur in the coming weeks to better get an idea of how this general model can constrained.

\section{W boson mass correction}

Motivated by the extreme tension between the W boson mass measurement made by CDF II \cite{CDF:2022hxs} and those made previously, a new model that allows for modification of the W boson mass has been outlined \cite{FileviezPerez:2022lxp}. Crucially, this model naturally modifies the W boson mass without affecting the Z boson mass, hence the standard model Z boson remains unaltered.

The simple model, $\Sigma$SM introduces a real scalar triplet to the standard model. In doing so, two additional scalar particles are introduced, a CP-even neutral Higgs, and a charged Higgs.

The scalar triplet introduced has a vacuum expectation value (VEV), $x_0$, that is distinct from the SM Higgs VEV, $v_0$. In the model described here, the mass of the W boson is given by

$$
M_W^2 = (M_W^{SM})^2 + g_2^2x_0^2,
$$

\noindent thus the W boson mass can vary with $x_0$.

An elegant feature of this model is that the Z boson mass does not deviate from its standard model value. This is because the scalar triplet VEV does not contribute to the Z boson mass since there is no coupling of the form $(\sigma^0)^2Z_{\mu}Z^{\mu}$.


\section{Drell-Yan production in third-generation gauge vector leptoquark models}

The main motivation for this model \cite{Haisch:2022afh} was the hint at new physics offered in early 2022 by the LHCb collaboration, specifically in results suggesting violation of lepton flavour universality \cite{LHCb:2021trn}. However, towards the end of 2022, further analysis brought the measurement back in line with lepton universality \cite{LHCb:2022zom,LHCb:2022qnv}.

Although much of the motivation for this model is now gone, the article outlines improvements to the theoretical description of Drell-Yan dilepton production in models with a single vector leptoquark.

With this model, I plan to put its applicability and driving motivation into context, with reference to the LHCb results. In the research essay, I would like to explore this model in more depth and demonstrate its value, despite the motivation for its proposal being tempered rather harshly.

\printbibliography[title={References}]

\end{document}